Commerce Check
==============

This module adds a pay by check payment method.

### INSTALLATION
Install as usual, see [Installing Drupal 8 Modules](https://www.drupal.org/node/1897420) or [Installing modules' Composer dependencies](https://www.drupal.org/node/2627292) for further information.

### SPONSORS
- [Rosewood Marketing](http://www.rosewood.us.com)

### CONTACT
Developed and maintained by Cambrico ([http://cambrico.net](http://cambrico.net)).

Get in touch with us for customizations and consultancy: [http://cambrico.net/contact](http://cambrico.net/contact)

#### Current maintainers:
  - Pedro Cambra [(pcambra)](http://drupal.org/user/122101)
  - Manuel Egío [(facine)](http://drupal.org/user/1169056)
