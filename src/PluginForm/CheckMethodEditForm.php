<?php

namespace Drupal\commerce_check\PluginForm;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a class for check payment gateway plugin edit form.
 */
class CheckMethodEditForm extends CheckMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $form['payment_details']['check_number']['#default_value'] = $payment_method->get('check_number')->value;
    $form['payment_details']['check_date']['#default_value'] = $payment_method->get('check_date')->value;

    return $form;
  }

}
