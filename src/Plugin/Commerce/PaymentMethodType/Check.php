<?php

namespace Drupal\commerce_check\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the check payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_check",
 *   label = @Translation("Check"),
 * )
 */
class Check extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@check_number' => $payment_method->check_number->value,
      '@check_date' => $payment_method->check_date->value,
    ];
    return $this->t('@check_number on @check_date', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['check_number'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Check number'))
      ->setDescription($this->t('The digits of the check number'))
      ->setRequired(TRUE);

    $fields['check_date'] = BundleFieldDefinition::create('datetime')
      ->setLabel($this->t('Check Date'))
      ->setDescription($this->t("The check's date"))
      ->setRequired(TRUE)
      ->setSetting('datetime_type', 'date');

    return $fields;
  }

}
